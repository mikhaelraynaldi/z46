$(function() {
	// body...

					$(document).ready(function() {
						$('#produk_post').DataTable({

						});
					});

					$('.btn-hapus').click(function() {
						var confirmasi = confirm("Yakin Ingin Mengahapus ?");
						if (confirmasi) {
							var id_produk = $(this).attr('data-product-id');


							let data = {
								id: $(this).attr('data-product-id')
							};
							// console.log(data);

							$.ajax({
								url: 'http://localhost/z46/post/delete_product',
								type: 'post',
								data: data,

								success(res) {
									console.log(res);
									location.reload();
								}

							});
						}
					});

					// Function yang dijalankan ketika
					// element dengan class '.btn-lihat' di CLICK
					$('.btn-lihat').click(function() {
						// Mendefinisikan variabel 'req_data'
						// dengan isi:
						// 'id': (class: $(this)||.btn-lihat) (attribute: data-id)
						// untuk dikirimkan melalui AJAX
						let req_data = {
							id: $(this).attr('data-id')
						};

						// Request AJAX
						$.ajax({
							// URL tujuan request AJAX
							url: 'http://localhost/z46/post/ajax_get_post_html',

							// Tipe request AJAX (GET)
							type: 'get',

							// Mengirimkan variable 'req_data' melalui AJAX
							data: req_data,

							// Function yang dijalankan jika request 'success'
							// Menerima parameter 'response' dari return AJAX
							success: function(response) {
								// 1. Cari element '#modal-post'
								// 2. Di dalam element '#modal-post', cari element '.modal-body'
								// 3. Jalankan fungsi 'html' untuk mengganti konten
									// di dalam element '.modal-body' tsb
									// dengan variabel 'response'
								$("#modal-post")
									.find('.modal-body')
									.html(response);
							}

						})
					});
});
		