$(document).ready(function() {
		$('#contactlist').DataTable();
	});
	// Function di jalankan ketika
	// element '.btn-warning' di click
	$('.btn-warning').click(function() {
		// Mendefinisikan variabel data dengan isi :
		// 'id' : (class : $this||.btn-warning ) (Attribute : data-id)
		// untuk dikirimkan menlalui ajax
		let data_reques = {
			id: $(this).attr('data-id')
		}
	// Request Ajax
	$.ajax({
		// url untuk mengirimkan ke tujuan yang di request
		url: 'http://localhost/z46/contactlist/ajax_get_contact_html',
		// Tipe Request Ajax (get)
		type: 'get',
		// Mengirimkan variabel date_reques melalui ajax
		data : data_reques,
	
		// Function yang di jalankan jika request 'success'
		// menerima parameter 'res' dari return ajax
		success: function(res){
			// 1. mencari '#modal-detail'
			// 2. di dalam '#modal-detail', cari element '.modal-body'
			// 3. jalankan fungsi 'html' untuk mengganti kontent di dalam elemenet '.modal-body'
			// dengan variabel 'res'
				$('#modal-detail').find('.modal-body').html(res);
		}
	});
});

