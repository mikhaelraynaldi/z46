$(function(){
	$('#cancel_button').click(function(e) {
				e.preventDefault();

				var confirmed = confirm("Yakin Ingin Membatalkan ?");
				if (confirmed) {
					window.location.href = "http://localhost/z46/post"
				}
			});

			$('#post_product').on('submit', function(e) {
				e.preventDefault();
				let type = $(this).attr('method');

				// Mendefinisikan variabel formData
				// yang di isi dengan class FormData
				let formData = new FormData();

				

				// Menyambungkan index 'file', dengan nilai
				//'#gambar_produk' dengan index ke 0 dan file index ke 0
				// yang berisi nilai binary
				formData.append('file', $("#gambar_produk")[0].files[0]);

				//Menyambukan index 'form' dengan nilai
				// semua tag input yang ada di #post_product
				// dengan attribute 'name' sebagai index untuk di kirim melalui AJAX
				formData.append('form', $(this).serialize());
				// console.log($(this).serialize());



				// // Medefinisikan variabel edit_data
				// // Dengan isi : Dari id post_product
				// // Mencari attribute name untuk di kirim melalui AJAX
				// let edit_data = $(this).serialize();

				$.ajax({
					url: 'http://localhost/z46/post/ajax_update_post',
					type: type,
					data: formData,

					// Mencegah konversi data menjadi string secara otomatis
					processData: false,

					// Menghilangkan batasan string yang ada dari header
					contentType: false,

					success :function(res) {
						window.location.href = "http://localhost/z46/post" //untuk mendirect ke page yang lain
					}
				})
				return false;
			});
});