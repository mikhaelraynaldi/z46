$(function() {
	$('#contact_form').on('submit', function() {
      // Nama variabel type , this itu adalaha dari (contact_form), lalu attr adalah attribute method yang ada di form
      // lalu disimpan di variabel let 
      let type = $(this).attr('method');

      //let data adalah variabel yang memebentuk object yang berisi nama, email , subject, pesan
      let data = {
        nama: $(this).find('[name=nama]').val(),
        email: $(this).find('[name=email]').val(),
        subject: $(this).find('[name=subject]').val(),
        pesan: $(this).find('[name=pesan]').val()
        // pada object ini berisi this yang artiny dari id contact_form lalu mencari setiap name yang
        // memiliki name : nama , email , subject ,pesan
      };

      // console.log(data);
      // Lalu di kirim menggukan ajax
      $.ajax({
        url: "http://localhost/z46/contact/input_contact", // ke alamt yang di tunju 
        type: type, // tipe nya tipe dengan yang tadi di atas
        data: data, // data ny berasal dari variabel data

        //hasilnya akan di kirim kembali oleh controller ke function()
        //contohnya function(res)
        success: function(res) {
          let success = res;
          //lalu dibuat variabel success untuk mengambil dari res yang ada 
          // di function(res)

          // jika yang di kembalikan controller hasilny berhasil atau 1
          //maka jalankan           
          if (success == 1) {
            alert('Pesan berhasil dikirimkan.');
            location.reload();
            // jika bukan jalankan else
          } else {
            alert('Gagal mengirimkan pesan');
          }
        }
      });
      return false;
    });
});