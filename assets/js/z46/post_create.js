$(function(){
$('#cancel_button').click(function(e) {
				e.preventDefault();

				var confirmed = confirm("Yakin Ingin Membatalkan ?");
				if (confirmed) {
					window.location.href = "http://localhost/z46/post"
				}
			});

			$('#post_product').on('submit', function(e) {
				e.preventDefault();
				// Mendefinisikan variabel formData
				// yang di isi dengan classs FormData
				let formData = new FormData();

				let type = $(this).attr('method');
				let data = {
					brand: $(this).find('[name=merekproduk]').val(),
					type: $(this).find('[name=tipeproduk]').val(),
					name: $(this).find('[name=namaproduk]').val(),
					description: $(this).find('[name=deskripsi]').val(),
					price: $(this).find('[name=harga]').val()
				};

				// Menyambungkan index 'file' , dengan nilai
				// '#gambar_produk' dengan index ke 0 dan files index ke 0
				// yang berisi nilai binary
				formData.append('file', $("#gambar_produk")[0].files[0]);

				// Menyambungkan index 'form' dengan nilai 
				// Semua tag input yang ada di #post_product
				// dengan attribute 'name' sebagai index untuk di kirim melalui AJAX
				formData.append('form', $(this).serialize());

				$.ajax({
					url: 'http://localhost/z46/post/save_product',
					type: type,
					data: formData,
					// Mencegah konversi data menjadi string secara otomatis
					processData: false,

					// Menghilangkan batasan string yang ada dari header
					contentType: false,

					success: function(res) {
						// untuk redirect ke page lain: "http://localhost/z46/post"
						window.location.href = "http://localhost/z46/post"
					}
				})
				return false;
			});
});