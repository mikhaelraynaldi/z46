<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_order extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	// Accept : No Parameter
	// Return : Array (all order)
	public function get_all_order()
	{
		// Select data from database
		// Table : order
		$hasil = $this->db
		->get('orders')
		->result_array();

		// return array all order
		return $hasil;

	}

	// ACCEPT $id (ORD_NUM Order)
	// Return: Array (1 order)
	public function get_one_order($id)
	{
		// Select data from DB
		// Table : orders
		$hasil = $this->db
		->where('ORD_NUM', $id)
		->get('orders')
		->result_array();

		// return : arrya (all order)
		return $hasil[0];
	}

}

/* End of file M_order.php */
/* Location: ./application/models/M_order.php */