<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_product extends CI_Model {

	public $variable;
	// Mendefiiniskan variabel $max_products_per_page 
	// Secara private
	private $max_products_per_page;

	public function __construct()
	{
		parent::__construct();
		//Mendefinisikan nilai jumlah maxilam product dalam 1 page
		$this->max_products_per_page = 12;
	}

	// // Accept : No Parameter
	// // Return : all array (product)
	// public function get_all_product()
	// {
	// 	// Select data from db
	// 	// Table : product
	// 	$dataproduk = $this->db
	// 		->get('product')
	// 		->result_array();

 //       // Return Array All Product
 //       return $dataproduk;
	// }

	// Accept: $id (ID Product)
	// Return : Array (1 product)
	public function get_one_product($id)
	{
		// Select data from DB
		// Table: product
		$hasil = $this->db
		->where('id', $id)
		->get('product')
		->result_array();

		// Return: Array (1 Product)
		return $hasil[0];
	}

	public function get_all_brand($brand)
	{
		// Select data from DB
		// Table: product
		$query = $this->db
                ->where('brand', urldecode($brand))// urldecode: mengconvert string 
                ->get('product')
                ->result_array();

        // Return : array (All brand selected)        
        return $query;
	}

	// public function get_all_type($type)
	// {
	// 	// Select data from DB
	// 	// Table: product
	// 	$query = $this->db
 //                ->where('type', urldecode($type))// urldecode: mengconvert string 
 //                ->get('product')
 //                ->result_array();

 //        // Return : array (All type selected)        
 //        return $query;
	// }

	
	public function get_by_page($page_number) {
		// Mendefinisikan variabel $limit 
		// dengan isi $this->max_products_per_page
		$limit = $this->max_products_per_page;

		// Mendefinisikan variabel $offset dengan isi 
		// $page_number - 1 lalu di * dengan $limit
		$offset = $page_number * $limit - $limit;

		// Select data from db
		// Table : product
		$query = $this->db->from('product');

		//Jika variabel $page_number 
		//tidak sama dengan null
		if($page_number != null)

			//Melalukan query limit 
			// Dengan Variabel $query, $limit dan $offset
			// Yang sudah disediakan
			$query->limit($limit, $offset);

		// Mendefinisikan variabel $products
		// Dengan isi : $query->get()->result_array();
		// Artinya : Select data dari db 
		// pada tabel product lalu get semua 
		// di jadikan array
		$products = $query->get()->result_array();

		return $products;
	}

	public function get_by_page_type($page_number, $type) {
		// Mendefinisikan variabel $limit 
		// dengan isi $this->max_products_per_page
		$limit = $this->max_products_per_page;

		// Mendefinisikan variabel $offset dengan isi 
		// $page_number - 1 lalu di * dengan $limit
		$offset = $page_number * $limit - $limit;

		// Select data from db
		// Table : product
		$query = $this->db->where('type', urldecode($type))->from('product');

		//Jika variabel $page_number 
		//tidak sama dengan null
		if($page_number != null)

			//Melalukan query limit 
			// Dengan Variabel $query, $limit dan $offset
			// Yang sudah disediakan
			$query->limit($limit, $offset);

		// Mendefinisikan variabel $products
		// Dengan isi : $query->get()->result_array();
		// Artinya : Select data dari db 
		// pada tabel product lalu get semua 
		// di jadikan array
		$products = $query->get()->result_array();

		return $products;
	}

	public function get_by_page_brand($page_number, $brand) {
		// Mendefinisikan variabel $limit 
		// dengan isi $this->max_products_per_page
		$limit = $this->max_products_per_page;

		// Mendefinisikan variabel $offset dengan isi 
		// Rumus untuk menentukan offset
		$offset = $page_number * $limit - $limit;

		// Select data from db
		// Table : product
		$query = $this->db->where('brand', urldecode($brand))->from('product');

		//Jika variabel $page_number 
		//tidak sama dengan null
		if($page_number != null)

			//Melalukan query limit 
			// Dengan Variabel $query, $limit dan $offset
			// Yang sudah disediakan
			$query->limit($limit, $offset);

		// Mendefinisikan variabel $products
		// Dengan isi : $query->get()->result_array();
		// Artinya : Select data dari db 
		// pada tabel product lalu get semua 
		// di jadikan array
		$products = $query->get()->result_array();

		return $products;
	}

	public function count_pages() {
		// Select data from db
		// Table : product
		$query = $this->db->get('product');

		// Mendefinisikan variabel $row_number
		// dengan isi : Mengambil jumlah baris yang di kembalikan
		// dari tabel product
		$row_number = $query->num_rows();

		//Mendefinisikan variabel $page_count
		// Dengan isi : ceil yang gunanya menjumlah pembulatan ke atas 
		// $row_number di bagi dengan $this->max_products_per_page
		$page_count = ceil($row_number / $this->max_products_per_page);

		return $page_count;
	}
}

/* End of file M_product.php */
/* Location: ./application/models/M_product.php */