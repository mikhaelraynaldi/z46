<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class M_contactlist extends CI_Model
{
	public $variable;

	public function __construct()
	{
		parent::__construct();
	}

	//Accept : No Parameter
	//Return : Array (All Contact)
	public function get_all_contactlist()
	{
		// Select Data From DB
		// Table: Contact
		$hasil = $this->db->get('contact')->result_array();
		
		//Return : Array (All Contacrlist)
		return $hasil;

	}


	//Accept : No Parameter
	// Return: Array (1 post)
	public function get_one_contactlist($id)

	{
		// Select From DB 
		// Tabel : Contact
		$result = $this->db
			->where('id',$id)
			->get('contact')
			->result_array();

		// Return : Array (1 contactlist)
		return $result[0];
	}
}