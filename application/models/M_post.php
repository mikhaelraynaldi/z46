<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_post extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
	}

	// Accept: no parameter
	// Return: Array (all posts)
	public function get_all() {

		// Select data from DB
		// Table: product
		$result = $this->db
			->get('product')
			->result_array();

		// Return: Array (all posts)
		return $result;
	}

	// Accept: $id (ID )
	// Return: Array (1 post)
	public function get_one($id) {
		// Select data from DB
		// Table: product
		$result = $this->db
			->where('id', $id)
			->get('product')
			->result_array();

		// Return: Array (1 posts)
		return $result[0];
	}

}

/* End of file M_post.php */
/* Location: ./application/models/M_post.php */