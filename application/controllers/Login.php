<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    // Purpose : Show Login Page
    // Return : View (html)
    public function index()
    {
        $this->load->view('dashboard/login');
    }

    public function validate_login()
    {
        // Menerima parameter 'username' dan 'password'
        $username = $this->input->post('username');
        $pass = $this->input->post('password');

        // Melakukan pengecekan apakah ada username '$username'
        // yang ada di table database 'users'
        // Masukkan hasil query baris pertama (0) ke variabel '$user'
        $user = $this->db
            ->where('username', $username)
            ->get('users')
            ->row_array(0);

        // Jika variabel '$user' ditemukan / ada, lakukan:
        if($user){
            // Membandingkan password di database "$user['password']"
            // dengan password yang diberikan lewat form '$pass'
            $passwordtrue = $user['password'] == md5($pass);

            // Jika ada maka dapat masuk ke page dashboard
            if ($passwordtrue){

                // mendefinisikan index 'login' 
                $data_session = [
                    'login'     => true,
                    'username'  => $user['username'],
                    'full_name' => $user['full_name']
                ];
                
                $this->session->set_userdata($data_session);
                redirect(base_url('dashboard'));
            } else {
                $this->session->set_flashdata('login_error_message', true);
                redirect(base_url('login'), 'refresh');
            }

        }
    }

    public function close() {
        // Melakukan log out
        $this->session->sess_destroy();
        //Diredirect ke halaman login 
        redirect(base_url('login'), 'refresh');
    }


}
