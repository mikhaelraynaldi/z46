<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Contactlist extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('login')!= true)
            redirect('login', 'refresh');
    }

    //Purpose: Show All ContactList
    //Result: View (Html)
    public function index()
    {
        //Load : Model M_contactlist.php
        //Alias : $this->contactlist
         $this->load->model('M_contactlist','contactlist');

         // Call : function get_all_contactlist
         // Parameter : No Parameter
         // From : file M_contactlist.php
         $hasil = $this->contactlist->get_all_contactlist();

         //Mendefinisikan variabel $data 
         // untuk dikirimkan ke view
         $data = [];

         // mendefinisikan index 'contact' pada variabel $data
         // dengan isi variabel $hasil
        $data['contact'] = $hasil;
        $data['title'] =  'Contact List';


        //Mengirimkan dan menampilkan view 'dashboard/contactlist.php'
        //mengirimkan variabel $data ke view
        $this->load->view('dashboard/templated/header', $data);
        $this->load->view('dashboard/contactlist', $data);
        $this->load->view('dashboard/templated/footer');


        // $this->load->view('dashboard/templated/header');
        // $this->load->view('dashboard/templated/footer');
    }

    //Purpose: Show 1 Contact List
    // Result : View (Html)
    public function ajax_get_contact_html()
    {

        // 1. Menerima variabel yang di kirimkan dari javascript
        // mendefinisikan variabel $id dengan isi parameter 'id' dari request ajax
        $id = $this->input->get('id');

        //2. ambil post dengan id = $id dari database
        // load : Model file M_contactlist.php
        // Allias : $this->contactlist
        $this->load->model('M_contactlist', 'contactlist');

        // call : function get_one_contactlist()
        // from : M_contactlist.php
        // dengan parameter $id
       $hasil = $this->contactlist->get_one_contactlist($id);

       // mendefinisikan index 'contactlist' pada variabel $data
       // dengan isi vairbale $hasil
        $data ['contactlist'] = $hasil;

        // buka dan tampilan view dengan data (1)
        $this->load->view('dashboard/contactlist_single', $data);



    }


}
