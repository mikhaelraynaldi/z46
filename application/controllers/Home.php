<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $hasilproduk = $this->db->get('product')->result_array();
        $data['produk'] = $hasilproduk;
        $this->load->view('home/front/header');
        $this->load->view('home/index', $data);
        $this->load->view('home/front/footer');
    }
}
