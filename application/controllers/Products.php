<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Products extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    // Purpose: show all post
    // return: view (html)
    public function index()
    {   
        // Menerima parameter yang dikirimkan
        // Dengan isi : index  'p'
        $page_number = $this->input->get('p');

        //Load: model M_product.php
        // alias : $this->product
        $this->load->model('M_product','product');

        // call : function get_all_product()
        //  parameter: none
        // from : M_product.php
        $hasil = $this->product->get_by_page($page_number);

        // Mendefinisikan variabel $page_count dengan isi 
        // Memangil method yang ada di file M_product
        $page_count = $this->product->count_pages();

        // mendefinisikan variabel $data
        // untuk di kirimkan ke view
        $data = [];

        // Mendefinisikan index 'produk'
        $data['produk'] = $hasil;

        $data['title'] = "Daftar Produk";
        $data['page_count'] = $page_count;
        $data['current_page'] = $page_number;

        // Membuka dan menampilkan view 'home/front/header.php, home/products.php, home/front/footer.php'
        // Mengirimkan variabel $data ke view
        $this->load->view('home/front/header', $data);
        $this->load->view('home/products', $data);
        $this->load->view('home/front/footer');
    }

    // Purpose : show 1 product
    // Retunr: View (HTML)
    public function product_single()
    {
        // 1. Terima variabel yang di kirimkan oleh javascript
        // mendefinisikan variabel $id
        // dengan isi : parameter 'id' dari request AJAX
        $id = $this->input->get('id');
  


        // 2. Ambil product dengan id = $id dari database
             // Load : model M_product.php 
             // Alias : $this->product
        $this->load->model('M_product', 'product' );

        // Call: Function get_one_product()
        // From : M_product.php
        // Parameter : $id
        $hasil = $this->product->get_one_product($id);

        //  Mendefinisikan index 'produk' pada variabel $data
        // dengan isi variabel $hasil
        $data['produk'] = $hasil;
        $data['title'] = 'Detail Produk';

        //3. Buka dan tampilkan view dengan data (1)
        $this->load->view('home/front/header',$data);
        $this->load->view('home/product_single', $data);
        $this->load->view('home/front/footer');
    }

    public function product_brand()
    {
        $page_number = $this->input->get('p');
        $brand = $this->uri->segment(3);

        // echo $brand;

        $this->load->model('M_product', 'product');

        $hasil = $this->product->get_by_page_brand($page_number,$brand);
        $page_count = $this->product->count_pages();



        $data['produk'] = $hasil;
        $data['title'] = urldecode($brand);
        $data['page_count'] = $page_count;
        $data['current_page'] = $page_number;

        $this->load->view('home/front/header', $data);
        $this->load->view('home/product_brand', $data);
        $this->load->view('home/front/footer');

    }

    public function product_type()
    {
        $page_number = $this->input->get('p');
        $type = $this->uri->segment(3);

        // echo $brand;

        $this->load->model('M_product', 'product');

        $hasil = $this->product->get_by_page_type($page_number,$type);

         // Mendefinisikan variabel $page_count dengan isi 
        // Memangil method yang ada di file M_product
        $page_count = $this->product->count_pages();
        

        $data['produk'] = $hasil;
        $data['title'] = urldecode($type);
        $data['page_count'] = $page_count;
        $data['current_page'] = $page_number;

        $this->load->view('home/front/header', $data);
        $this->load->view('home/product_type', $data);
        $this->load->view('home/front/footer');

    }

}
