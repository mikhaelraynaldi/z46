<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class About extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data ['title'] =  'About';
        $this->load->view('home/front/header', $data);
        $this->load->view('home/about');
        $this->load->view('home/front/footer');
    }
}
