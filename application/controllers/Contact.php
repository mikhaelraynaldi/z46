<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['title'] = 'Contact Us';

        $this->load->view('home/front/header', $data);
        $this->load->view('home/contact');
        $this->load->view('home/front/footer');
    }

    public function input_contact()
    {
        //untuk menangkap data dari yang dikirim melalui ajax
        $nama = $this->input->post('nama');
        $email = $this->input->post('email');
        $subject = $this->input->post('subject');
        $pesan = $this->input->post('pesan');

        // di jadikan array
        $data = array(
            'nama' => $nama,
            'email' => $email,
            'subject' => $subject,
            'pesan' => $pesan
        );

        // true kalau berhasil
        // false kalau gagal
        $success = $this->db->insert('contact', $data);

        echo $success;
    }
}
