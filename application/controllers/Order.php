
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Standarr MVC
// (Model, View , Controller)
class Order extends CI_Controller {

	
	public function __construct()
	{
		parent::__construct();
	}

	// Show : Page Order
	// Return : view (html)
	public function index()
	{	
		//load: model M_order.php
		// alias : $this->order
		$this->load->model('M_order','order');

		// Call function get_all_order()
		// no parameter 
		// From M_order.php
		$hasil = $this->order->get_all_order();

		// mendefinisikan variabel $data
		// untuk di kirimkan ke view
		$data = [];

		// medefinisikan index 'order' pada variabel $data
		// yang berisi variabel $hasi
		$data['order'] = $hasil;

		// membuka dan menampilkan view 'home/order'
		// mengirim variabel $data ke view
		$this->load->view('dashboard/order', $data);
		
	}


	//Purpose : Show 1 pose
	// Return : view (html)
	public function ajax_get_order_html()
	{
		// 1. Terima variabel yang dikirimkan dari javascript
		// mendefinisikan variabel $id
		// dengan isi parameter 'ORD_NUM' dari request ajax
		$id = $this->input->get('ORD_NUM');

		//load : model M_order.php
		//alias : $this->order
		$this->load->model('M_order', 'order');

		// call function get_one_order
		// from : M_order.php
		// parameter: $id
		$hasil = $this->order->get_one_order($id);

		// mendefinisikan index 'order' pada variabel $data
		// yang berisi variabel $hasil
		$data['order'] = $hasil;

		// 3. Buka dan tampilkan view dengan data (1)
		$this->load->view('dashboard/order_single', $data);

	}
}

/* End of file Order.php */
/* Location: ./application/controllers/Order.php */