<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Gallery extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        $data['title'] =  'Gallery';
        $this->load->view('home/front/header', $data);
        $this->load->view('home/gallery');
        $this->load->view('home/front/footer');
    }
}
