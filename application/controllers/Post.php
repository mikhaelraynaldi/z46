<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// STANDARD MVC
// (Model, view, controller)
class Post extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('login') != true)
            redirect('login', 'refresh');
        

    }

    // Purpose: show all posts
    // Return: View (HTML)
    public function index()
    {
        // Load: model M_post.php
        // Alias: $this->post
        $this->load->model('M_post', 'post');

        // Call: function get_all()
        // Parameter: none
        // From: M_post.php
        $result = $this->post->get_all();

        // Mendefinisikan variabel $data
        // untuk dikirimkan ke view
        $data = [];

        // Mendefinisikan index 'product' pada variabel $data
        // dengan isi variabel $result
        $data['product'] = $result;

        $data['title'] = 'Post';

        // Membuka dan menampilkan view 'dashboard/post.php'
        // Mengirimkan variabel $data ke view
        $this->load->view('dashboard/templated/header', $data);
        $this->load->view('dashboard/post', $data);
        $this->load->view('dashboard/templated/footer');
    }

    public function post_create()
    {
        $data['title'] = 'Post Create';
        $this->load->view('dashboard/templated/header', $data);
        $this->load->view('dashboard/post_create');
        $this->load->view('dashboard/templated/footer');


    }

    public function post_edit() {
        // Load: model M_post.php
        // Alias: $this->post
        $this->load->model('M_post', 'post');

        // Menerima parameter dari segment ke 3 di URL
        // sebagai ID post ($post_id)
        $post_id = $this->uri->segment(3);

        // Call: function get_one()
        // From: M_post.php
        // Parameter: $post_id
        $data['post'] = $this->post->get_one($post_id);
        $data['title'] =  'Post Edit';

        // Membuka dan menampilkan view 'dashboard/post_edit.php'
        // Mengirimkan variabel $data ke view
        $this->load->view('dashboard/templated/header', $data);
        $this->load->view('dashboard/post_edit', $data);
        $this->load->view('dashboard/templated/footer');


    }

    public function save_product()
    {
        // Menerima parameter dengan index '$form'
        // di-assign ke variabel '$params'
        parse_str($this->input->post('form'), $params);

        // Menentukan directory lokasi file yang akan terupload
        $config['upload_path']   = './assets/uploads/';

        // Membatasi tipe file yang boleh diupload
        $config['allowed_types'] = 'gif|jpg|png';

        $config['max_size'] = 500;//500 kb 

        // Mengubah nama file (terenkripsi / encrypted)
        // supaya tidak mengandung karakter yang tidak diizinkan
        $config['encrypt_name']  = true;

        // Memuat libary upload dengan konfigurasi
        // yang sudah di tentukan oleh variabel $config
        $this->load->library('upload', $config);

        // Mendefinisikan variabel image_url : null
        $image_url = null;

        // Melakukan proses uploading data dengan index 'file' (dari request)
        if ( !$this->upload->do_upload('file') ) {

            // Dijalankan jika proses uploading data
            // mengembalikan nilai 'false' (gagal)
            $error = [
                'message' => $this->upload->display_errors()
            ];

        } else {

            // Dijalankan jika proses uploading data berhasil 

            // Mengambil keterangan proses uploading dengan index 'file_name'
            // kemudian disimpan ke dalam variabel '$filename'
            $filename = $this->upload->data('file_name');

            // Men-generate text base URL, disambung dengan nama direktori lokasi uploading
            // dan nama file. Hasil kemudian disimpan ke dalam variabel '$image_url'
            $image_url = base_url('assets/uploads/' . $filename);
        }

        // Mentransformasi (mengubah bentuk) array '$params'
        // untuk disambung dengan variabel '$image_url'
        // kemudian disimpan dalam variabel '$data'
        $data = [
            // Mendefinisikan index sesuai nama kolom dari table 'product'
            'brand'       => $params['merekproduk'],
            'type'        => $params['tipeproduk'],
            'name'        => $params['namaproduk'],
            'description' => $params['deskripsi'],
            'price'       => $params['harga'],
            // Mendefinisikan index 'image_url' dengan isi: variabel '$image_url'
            'image_url'   => $image_url
        ];

        // Melakukan insert ke tabel product
        // dengan data dari variabel '$data'
        $this->db->insert('product', $data);
    }

    public function delete_product()
    {
        $id = $this->input->post('id');

        $data_hapus = [ 'id' => $id ];
        $this->db->delete('product', $data_hapus);

      
    }

    // Purpose: show 1 post
    // Return: View (HTML)
    public function ajax_get_post_html() {
        // 1. Terima variabel yang dikirimkan dari javascript
            // Mendefinisikan variabel $id
            // Dengan isi: parameter 'id' dari request AJAX
        $id = $this->input->get('id');

        // 2. Ambil post dengan id = $id dari database
            // Load: model M_post.php
            // Alias: $this->post
        $this->load->model('M_post', 'post');

            // Call: function get_one()
            // From: M_post.php
            // Parameter: $id
        $result = $this->post->get_one($id);

            // Mendefinisikan index 'post' pada variabel $data
            // dengan isi variabel $result
        $data['post'] = $result;

        // 3. Buka dan tampilkan view dengan data (1)
        $this->load->view('dashboard/post_single', $data);
    }

    //Purpose : Show 1 post
    // Return: View (html)
    public function ajax_update_post() {
        // Menerima parameter dengan index 'form'
        // di-assign ke variabel '$params'
        parse_str($this->input->post('form'), $params);

        // Mendefinisikan konfigurasi yang akan digunakan
        // oleh library 'upload'
        $config = [
            'upload_path' => './assets/uploads/', // Menentukan directory lokasi file yang akan terupload
            'allowed_types' => 'gif|jpg|png', // Membatasi tipe file yang boleh diupload
            'encrypt_name' => true, // Mengubah nama file (terenkripsi / encrypted), supaya tidak mengandung karakter yang tidak diizinkan
            'max_size' => 500, // Menentukan batasan size file (500KB)
        ];

        // Memuat library upload dengan konfigurasi
        // yang sudah di tentukan di variabel $config
        $this->load->library('upload', $config);

        // Mendefinisikan variabel $image_url
        // dengan nilai kosong
        $image_url = null;

        // Jika file yang diupload tidak kosong
        if(count($_FILES) > 0) {
            // Melakukan proses uploading data dengan index 'file' (dari request)
            if ( !$this->upload->do_upload('file') ) {

                // Dijalankan jika proses uploading data
                // mengembalikan nilai 'false' (gagal)
                $error = [ 'message' => $this->upload->display_errors() ];

            } else {
                // Dijalankan jika proses uploading data berhasil 

                // Mengambil keterangan proses uploading dengan index 'file_name'
                // kemudian disimpan ke dalam variabel '$filename'
                $filename = $this->upload->data('file_name');

                // Men-generate text base URL, disambung dengan nama direktori lokasi uploading
                // dan nama file. Hasil kemudian disimpan ke dalam variabel '$image_url'
                $image_url = base_url('assets/uploads/' . $filename);
            }
        }

        // Mentransformasi (mengubah bentuk) array '$params'\
        // kemudian disimpan dalam variabel '$data'
        $data = [
            // Mendefinisikan index sesuai nama kolom dari table 'product'
            'brand'       => $params['brand'],
            'type'        => $params['type'],
            'name'        => $params['name'],
            'description' => $params['description'],
            'price'       => $params['price']
        ];

        // Jika variabel '$image_url' tidak kosong
        // isi variabel '$data' pada index 'image_url'
        // dengan variabel '$image_url'
        if($image_url) {
            $data['image_url'] = $image_url;
        }


        // Mendefinisikan index sesuai nama kolom dari table 'product'
        $condition = [ 'id' => $params['id'] ];

        // //3. Buka dan tampilkan view dengan data (1)
        $this->db->update('product', $data, $condition);
    }
}
