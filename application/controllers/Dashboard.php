<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if($this->session->userdata('login') != true)
            redirect('login', 'refresh');
    }

    public function index()
    {
        
        $this->load->view('dashboard/templated/header');
        $this->load->view('dashboard/dashboard');
        $this->load->view('dashboard/templated/footer');

    }
}
