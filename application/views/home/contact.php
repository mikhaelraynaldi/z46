<main id="main">
  <!-- ======= Contact Section ======= -->
  <section id="contact" class="contact">
    <div class="container">

      <div class="section-title" data-aos="fade-up">
        <h2>Contact</h2>

      </div>

      <div class="row no-gutters justify-content-center" data-aos="fade-up">

        <div class="col-lg-5 d-flex align-items-stretch">
          <div class="info">
            <div class="address">
              <i class="icofont-google-map"></i>
              <h4>Location:</h4>
              <p>Jl. Raya Diklat Pemda, Desa Gerubug<br>
                Dukuh Pinang Bojong Nangka<br>Kelapa Dua Curug Tangerang.</p>
            </div>

            <div class="email mt-4">
              <i class="icofont-envelope"></i>
              <h4>Email:</h4>
              <p>z46battrey@gmail.com </p>
            </div>

            <div class="phone mt-4">
              <i class="icofont-phone"></i>
              <h4>Call Us:</h4>
              <p>081332249214</p>
            </div>

            <div class="phone mt-4">
              <i class="icofont-clock-time"></i>
              <h4>Opration:</h4>
              <p>Senin - Minggu : 07.30 - 20.00 WIB</p>
            </div>

          </div>

        </div>

        <div class="col-lg-5 d-flex align-items-stretch">
          <iframe style="border:0; width: 100%; height: 460px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15864.231756992218!2d106.60199232695788!3d-6.256097877546278!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69fdd7392d6d8b%3A0x363d2b6d17fbaf20!2sToko%20Z46%20Aki!5e0!3m2!1sen!2sid!4v1621231290118!5m2!1sen!2sid" frameborder="0" allowfullscreen></iframe>
        </div>

      </div>

      <div class="row mt-5 justify-content-center" data-aos="fade-up">
        <div class="col-lg-10">
          <form id="contact_form" method="post" role="form" class="php-email-form">
            <div class="form-row">
              <div class="col-md-6 form-group">
                <input type="text" name="nama" class="form-control" id="name" placeholder="Nama" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required="" />

              </div>
              <div class="col-md-6 form-group">
                <input type="email" class="form-control" name="email" id="email" placeholder="Email" data-rule="email" data-msg="Please enter a valid email" required="" />

              </div>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" required="" />

            </div>
            <div class="form-group">
              <textarea class="form-control" name="pesan" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Pesan"></textarea>
            </div>
            <!-- <div class="mb-3">
              <div class="loading">Loading</div>
              <div class="error-message"></div>
              <div class="sent-message">Your message has been sent. Thank you!</div>
            </div> -->
            <div class="text-center"><button type="submit">Send</button></div>
          </form>
        </div>

      </div>

    </div>
  </section><!-- End Contact Section -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="<?= base_url('assets/js/z46/contact.js');?>"></script>
</main>