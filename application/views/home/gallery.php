<main id="main">

  <!-- ======= About Lists Section ======= -->
  <section class="about-lists">
    <div class="section-title">
      <h2>Gallery</h2>
    </div>
    <div class="container">

      <div class="row no-gutters">

        <div class="col-lg-4 col-md-6 content-item" data-aos="fade-up">
          <div class="pic"><img src="assets/img/Gallery/d7.jpg" class="img-fluid" alt=""></div>
        </div>

        <div class="col-lg-4 col-md-6 content-item" data-aos="fade-up">
          <div class="pic"><img src="assets/img/Gallery/e1.jpg" class="img-fluid" alt=""></div>
        </div>

        <div class="col-lg-4 col-md-6 content-item" data-aos="fade-up">
          <div class="pic"><img src="assets/img/Gallery/e7.jpg" class="img-fluid" alt=""></div>
        </div>

        <div class="col-lg-4 col-md-6 content-item" data-aos="fade-up">
          <div class="pic"><img src="assets/img/Gallery/e8.jpg" class="img-fluid" alt=""></div>
        </div>

        <div class="col-lg-4 col-md-6 content-item" data-aos="fade-up">
          <div class="pic"><img src="assets/img/Gallery/furukawa-1.jpg" class="img-fluid" alt=""></div>
        </div>

        <div class="col-lg-4 col-md-6 content-item" data-aos="fade-up">
          <div class="pic"><img src="assets/img/Gallery/NGS.jpg" class="img-fluid" alt=""></div>
        </div>

        <div class="col-lg-4 col-md-6 content-item" data-aos="fade-up">
          <div class="pic"><img src="assets/img/Gallery/tas.png" class="img-fluid" alt=""></div>
        </div>

        <div class="col-lg-4 col-md-6 content-item" data-aos="fade-up">
          <div class="pic"><img src="assets/img/Gallery/tbp.jpg" class="img-fluid" alt=""></div>
        </div>

        <div class="col-lg-4 col-md-6 content-item" data-aos="fade-up">
          <!-- <div class="pic"><img src="assets/img/team/team-4.jpg" class="img-fluid" alt=""></div> -->
        </div>

      </div>

    </div>
  </section><!-- End About Lists Section -->


</main><!-- End #main -->