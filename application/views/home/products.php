<main id="main">

  <!-- ======= About Lists Section ======= -->
  <section class="about-lists">
    <div class="section-title">
      <h2>Products</h2>
      <div class="row">
        <div class="col-10 text-right">
          <ul class="product-pagination">

            <?php
              // Melakukan pengulangan
              for ($i=1; $i <= $page_count; $i++) {

                // Jika variabel $i sama nilainya dengan 
                // Variabel $current_page 
                if($i == $current_page) {
                  // tampilkan element li dengan class seleceted
                  echo "<li class='selected'>";
                } else {
                  // jika tidak tampilkan elememet li 
                  echo "<li>";
                }
                // Mengirimkan nilai ke controller products 
                echo "<a href='" . base_url('products?p=' . $i) . "'>";
                // menampilkan angka 
                echo $i;
                echo "</a>";
                echo "</li>";
              }
            ?>
          </ul>
        </div>
      </div>
    </div>
    <div class="container">

      <div class="row no-gutters">
        <?php for ($y = 0; $y < count($produk); $y++) : ?>
          <div class="col-lg-3 col-md-6 content-item text-center" data-aos="fade-up">
            <div class="pic">
              <img src="<?php echo ($produk[$y]['image_url']);?>" class="img-fluid" alt="" style="height: 150px; width:150px;">
            </div>
            <h4 style="font-family: basic;">
              <!-- Mengubah format number -->
              Rp <?php echo number_format($produk[$y]['price'], 0, ',', '.'); ?>
                <br>
              
            </h4>
            <h5 style="font-family: basic;">
              <?php echo "{$produk[$y]['name']}"; ?>
            </h5>
            
           
            <div style="margin-top : 20px">
              <a href="https://api.whatsapp.com/send?phone=6281332249214" class="btn btn-sm btn-info">
                <i class="fa fa-whatsapp"></i>
                &nbsp;Order
              </a>
              <a href="<?php echo base_url('products/product_single?id='. $produk[$y]['id']) ?>" class="btn btn-sm btn-warning btn-detail">
              <i class="fa fa-eye"></i> &nbsp;Detail
              </a>
            </div>
          </div>
        <?php endfor;  ?>
      </div>

      <div class="row">
        <div class="col-12 text-right">
          <ul class="product-pagination">

            <?php
              for ($i=1; $i <= $page_count; $i++) {
                
                if($i == $current_page) {
                  echo "<li class='selected'>";
                } else {
                  echo "<li>";
                }

                echo "<a href='" . base_url('products?p=' . $i) . "'>";
                echo $i;
                echo "</a>";
                echo "</li>";
              }
            ?>
          </ul>
        </div>
      </div>

    </div>
  </section><!-- End About Lists Section -->

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script>

</script>

</main><!-- End #main -->