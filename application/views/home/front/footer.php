  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-info">
           <a href="<?= base_url();?>"> <img src="<?= base_url('assets/img/logo.png'); ?>" style="width: 90px; height: 50px;"></img></a>
            <br>
            <br>
            <p>
              <br><br>
            </p>

          </div>

          <div class="col-lg-4 col-md-6 footer-info">
            <h3>Address</h3>
            <p>
              Jl. Raya Diklat Pemda, Desa Gerubug <br>
              Dukuh Pinang Bojong Nangka<br>
              Kelapa Dua Curug Tangerang.<br>
              <br>
            </p>
            <h3>Operational Hours</h3>
            <p>
              Senin - Minggu :<br> 07.30 - 20.00 WIB
            </p>

          </div>

          <div class="col-lg-4 col-md-6 footer-info">
            <h3>Contact</h3>
            <strong>Phones:</strong><br>

            <div class="icon"><i class="icofont-phone"> 081332249214</i></div>
            <div class="icon"><i class="icofont-phone"> 081292682314</i></div>
            <div class="icon"><i class="icofont-phone"> 082317927137</i></div>
            <div class="icon"><i class="icofont-phone"> 08568484561</i></div>
            <br>
            <strong>Email:</strong> z46battrey@gmail.com <br>
            
          </div>
        </div>
      </div>
    </div>


    <div class="container">
      <div class="copyright">
        &copy; 2021 <strong><span>Z46 Group Jaya</span></strong>.
      </div>
      <div class="credits">
        Web Developed by <a href="mikhaelraynaldi.github.io" target="__blank">Mikhael</a>
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="<?= base_url('assets/vendor/jquery/jquery.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/jquery.easing/jquery.easing.min.js'); ?>"></script>
  <!-- <script src="<?= base_url('assets/vendor/php-email-form/validate.js'); ?>"></script> -->
  <script src="<?= base_url('assets/vendor/jquery-sticky/jquery.sticky.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/venobox/venobox.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/waypoints/jquery.waypoints.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/counterup/counterup.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/owl.carousel/owl.carousel.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/isotope-layout/isotope.pkgd.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/aos/aos.js'); ?>"></script>

  <!-- Template Main JS File -->
  <script src="<?= base_url('assets/js/main.js'); ?>"></script>

  </body>

  </html>