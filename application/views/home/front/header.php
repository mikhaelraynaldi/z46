<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Toko Aki Tangerang <?php echo isset($title) ? '| ' . $title : '';?> </title>
  <meta content="" name="description">
  <meta content="" name="keywords">


  <!-- Favicons -->
  <link href="<?= base_url('assets/img/favicon-96x96.png'); ?>" rel="icon">
  <link href="<?= base_url('assets/img/apple-touch-icon.png'); ?>" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Vendor CSS Files -->
  <link href="<?= base_url('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <link href="<?= base_url('assets/vendor/icofont/icofont.min.css'); ?>" rel="stylesheet">
  <link href="<?= base_url('assets/vendor/boxicons/css/boxicons.min.css'); ?>" rel="stylesheet">
  <link href="<?= base_url('assets/vendor/animate.css/animate.min.css'); ?>" rel="stylesheet">
  <link href="<?= base_url('assets/vendor/venobox/venobox.css'); ?>" rel="stylesheet">
  <link href="<?= base_url('assets/vendor/owl.carousel/assets/owl.carousel.min.css'); ?>" rel="stylesheet">
  <link href="<?= base_url('assets/vendor/aos/aos.css'); ?>" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?= base_url('assets/css/style.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/custom.front.css'); ?>" rel="stylesheet">

</head>

<body>

  <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-none d-lg-block">
    <div class="container clearfix">
      <div class="contact-info float-left">
        <i class="icofont-envelope"></i><a href="mailto:z46battrey@gmail.com ">z46battrey@gmail.com</a>
        <i class="icofont-phone"></i>081332249214
      </div>
    </div>
  </section>

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container">

      <div class="logo float-left">
        <!-- <h1 class="text-light"><a href="index.html"><span>Mamba</span></a></h1> -->
        <!-- Uncomment below if you prefer to use an image logo -->
        <a href="<?= base_url(); ?>"><img src="<?= base_url('assets/img/logo.png'); ?>" alt="" class="img-fluid"></a>
      </div>

      <nav class="nav-menu float-right d-none d-lg-block">
     
          <ul>

          <li class=""><a href="<?= base_url(); ?>">Home</a></li>
          <li class="drop-down"><a href="<?= base_url('products?p=1'); ?>">Products</a>
            <ul>
              <li><a href="<?= base_url('products/product_brand/GS Astra?p=1' );?>">GS ASTRA</a></li>
              <li><a href="<?= base_url('products/product_brand/Incoe Gold?p=1' );?>">Incoe Gold</a></li>
              <li><a href="<?= base_url('products/product_brand/Yuasa?p=1' );?>">Yuasa</a></li>
              <li><a href="<?= base_url('products/product_brand/Massiv xp?=p1' );?>">Massiv XP</a></li>
              <li><a href="<?= base_url('products/product_brand/NGS?p=1' );?>">NGS</a></li>
              <li><a href="<?= base_url('products/product_brand/Fleet?p=1' );?>">Fleet</a></li>


            </ul>
          </li>
          <li><a href="<?= base_url('gallery'); ?>">Gallery</a></li>
          <li><a href="<?= base_url('about'); ?>">About</a></li>
          <li><a href="<?= base_url('contact'); ?>" class="get-started-btn d-none d-lg-block" style="color: aliceblue;">Contact Us</a></li>
          <li><a href="<?= base_url('contact'); ?>" class="d-block d-sm-none">Contact Us</a></li>
        </ul>


      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->