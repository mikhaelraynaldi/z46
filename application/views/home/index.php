  <section id="hero">
    <div class="hero-container">
      <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">

        <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

        <div class="carousel-inner" role="listbox">

          <!-- Slide 1 -->
          <div class="carousel-item active" style="background-image: url('assets/img/slide/search.jpg');">
            <div class="carousel-container">
              <div class="carousel-content container">
                <h2 class="animate__animated animate__fadeInDown">Mau Cari Aki Berkualitas ? </h2>
                <p class="animate__animated animate__fadeInUp">Kami menyediakan Aki yang berkualitas, Hubungi kami sekarang.</p>
                <a href="https://api.whatsapp.com/send?phone=6281332249214" target="__blank" class="btn-get-started animate__animated animate__fadeInUp scrollto"><i class="fa fa-whatsapp"></i>&nbsp; Hubungi Kami</a>
              </div>
            </div>
          </div>

          <!-- Slide 2 -->
          <div class="carousel-item" style="background-image: url('assets/img/slide/slide-3.jpg');">
            <div class="carousel-container">
              <div class="carousel-content container">
                <h2 class="animate__animated animate__fadeInDown">Temukan Aki Terbaik Dengan <br>Harga Terjangkau</h2>
                <p class="animate__animated animate__fadeInUp">Tersedia Produk Aki Pada Website Ini.</p>
                <a href="<?= base_url('products?p=1');?>" class="btn-get-started animate__animated animate__fadeInUp scrollto">Cari Produk </a>
              </div>
            </div>
          </div>

          <!-- Slide 3 -->
          <!-- <div class="carousel-item" style="background-image: url('assets/img/slide/slide-3.jpg');">
            <div class="carousel-container">
              <div class="carousel-content container">
                <h2 class="animate__animated animate__fadeInDown">Sequi ea ut et est quaerat</h2>
                <p class="animate__animated animate__fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
                <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Read More</a>
              </div>
            </div>
          </div> -->

        </div>

        <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon icofont-rounded-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon icofont-rounded-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>

      </div>
    </div>
  </section><!-- End Hero -->

  <main id="main">


    <!-- ======= About Lists Section ======= -->
    <section class="about-lists">
      <div class="section-title">
        <h2>Jenis Aki</h2>
      </div>
      <div class="container">

        <div class="row no-gutters" style="text-align : center;">
         
            <div class="col-lg-3 col-md-6 content-item" data-aos="fade-up">
              <div class="pic"><img src="assets/img/car.png" class="img-fluid" alt=""></div>
           
              <div style="margin-top : 20px">
                <a href="<?= base_url('products/product_type/Aki Mobil?p=1')?>" class="btn btn-info">
                 Aki Mobil
               
                </a>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 content-item" data-aos="fade-up">
              <div class="pic"><img src="assets/img/motor.jpg" class="img-fluid" alt=""></div>
             
              <h3></h3>
              <div style="margin-top : 20px">
                <a href="<?= base_url('products/product_type/Aki Motor?p=1')?>" class="btn btn-info">
                    Aki Motor
               
                </a>
              </div>
            </div>
             <div class="col-lg-3 col-md-6 content-item" data-aos="fade-up">
              <div class="pic"><img src="assets/img/genset.jpg" class="img-fluid" alt=""></div>
            
              <h3></h3>
              <div style="margin-top : 20px">
                <a href="<?= base_url('products/product_type/Aki Genset?p=1')?>" class="btn btn btn-info">
                    Aki Genset
               
                </a>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 content-item" data-aos="fade-up">
              <div class="pic"><img src="assets/img/folklift.jpg" class="img-fluid" alt=""></div>
              
              <div style="margin-top : 20px">
                <a href="<?= base_url('products/product_type/Aki Forklift?p=1')?>" class="btn btn-info">
                    Aki Forklift
               
                </a>
              </div>
            </div>




        </div>
        <!-- <div class="section-title" style="margin-top: 50px;">
          <button type="button" class="btn btn-info" onclick="window.location.href='products'">More Product</button>
        </div> -->

      </div>
    </section><!-- End About Lists Section -->
    <section class="counts section-bg">
      <div class="container">
        <div class="section-title">
          <h2>Free Delivery</h2>
        </div>
        <div class="section-title d-none d-lg-block">
          <div class="pic"><img src="assets/img/banner-web-tahta1.jpg" style="width: 600px; height:350px;" alt=""></div>
        </div>
        <div class="section-title d-block d-sm-none">
          <div class="pic"><img src="assets/img/banner-web-tahta1.jpg" style="width: 200px; height:150px;" alt=""></div>
        </div>

      </div>
    </section>


    <!-- ======= Services Section ======= -->
    <section id="services" class="services">
      <div class="container">

        <div class="section-title">
          <h2>Services</h2>
        </div>

        <div class="row">
          <div class="col-lg-6 col-md-6 icon-box" data-aos="fade-up">
            <div class="icon"><i class="icofont-card"></i></div>
            <h4 class="title"><a href="">Finance/Pembayaran</a></h4>
            <p class="description">Transfer, Qris, Cash Tunai </p>
          </div>
          <div class="col-lg-6 col-md-6 icon-box" data-aos="fade-up" data-aos-delay="100">
            <div class="icon"><i class="icofont-box"></i></div>
            <h4 class="title"><a href="">Shiping/Pengiriman</a></h4>
            <p class="description"> CBD ( Cash By Delivery) dan Delivery Order </p>
          </div>
          <!-- <div class="col-lg-4 col-md-6 icon-box" data-aos="fade-up" data-aos-delay="200">
            <div class="icon"><i class="icofont-earth"></i></div>
            <h4 class="title"><a href="">Sed ut perspiciatis</a></h4>
            <p class="description">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur</p>
          </div>
          <div class="col-lg-4 col-md-6 icon-box" data-aos="fade-up" data-aos-delay="300">
            <div class="icon"><i class="icofont-image"></i></div>
            <h4 class="title"><a href="">Magni Dolores</a></h4>
            <p class="description">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
          </div>
          <div class="col-lg-4 col-md-6 icon-box" data-aos="fade-up" data-aos-delay="400">
            <div class="icon"><i class="icofont-settings"></i></div>
            <h4 class="title"><a href="">Nemo Enim</a></h4>
            <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
          </div>
          <div class="col-lg-4 col-md-6 icon-box" data-aos="fade-up" data-aos-delay="500">
            <div class="icon"><i class="icofont-tasks-alt"></i></div>
            <h4 class="title"><a href="">Eiusmod Tempor</a></h4>
            <p class="description">Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi</p>
          </div> -->
        </div>

      </div>
    </section><!-- End Services Section -->
    <!-- ======= Counts Section ======= -->
    <!-- ======= Frequently Asked Questions Section ======= -->
    <section id="faq" class="faq section-bg">
      <div class="container">

        <div class="section-title" data-aos="fade-up">
          <h2>F.A.Q</h2>

        </div>

        <div class="faq-list">
          <ul>
            <li data-aos="fade-up">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" class="collapse" href="#faq-list-1">Apakah barang bisa diantar ?<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-1" class="collapse" data-parent=".faq-list">
                <p>
                  Ya, dengan senang hati kami antar ke tujuan anda.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="100">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-2" class="collapsed">Apakah bisa melakukan pembayaran dengan cara transfer ? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-2" class="collapse" data-parent=".faq-list">
                <p>
                  Ya, Toko Aki Z46 dapat melakukaan pembayaran dengan cara transfer.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="200">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-3" class="collapsed">Apakah ada perbandingan harga antar merek aki ? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-3" class="collapse" data-parent=".faq-list">
                <p>
                  Ya, Disini tersedia perbandingan harga antara merek aki.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="300">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-4" class="collapsed">Apakah bisa bayar di tempat?<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-4" class="collapse" data-parent=".faq-list">
                <p>
                  Ya, tergantung melihat posisi dan syarat tertentu.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="400">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-5" class="collapsed">Apakah tersedia garansi ditoko ini ?<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-5" class="collapse" data-parent=".faq-list">
                <p>
                  Ya, tersedia garansi.
                </p>
              </div>
            </li>
            <li data-aos="fade-up" data-aos-delay="500">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-6" class="collapsed">Apakah ada layanan Jumper ?<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-6" class="collapse" data-parent=".faq-list">
                <p>
                  Ya, ada.
                </p>
              </div>
            </li>

          </ul>
        </div>

      </div>
    </section><!-- End F.A.Q Section -->

  </main><!-- End #main -->