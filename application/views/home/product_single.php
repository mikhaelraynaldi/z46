<main id="main">
  <!-- ======= About Lists Section ======= -->
  <section id="about" class="about">
    <div class="section-title">
      <h2>Products</h2>
      
    </div>
    
    <div class="container">
      <div class="row no-gutters">
        <div class="col-lg-6 d-flex flex-column justify-content-center about-content text-center">
          <div class="pic">
            <img src="<?php echo ($produk['image_url']);?>" class="img-fluid " alt="">
          </div>
        </div>
        <div class="col-lg-6 d-flex flex-column justify-content-center about-content">
          <div class="section-title">
            <h3 style="font-family: basic;">
            <?php echo $produk['name']?>
            </h3>
          </div>
          <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
            <h5 style="font-family: basic;">Harga : Rp
            <?php echo number_format($produk['price']);?>
            </h5>
            <h5 style="font-family: basic;">
            Merek :
            <?php echo $produk['brand']?>
            </h5>
            <h5 style="font-family: basic;">
            Jenis Aki :
            <?php echo $produk['type']?>
            </h5>
            
            <h6 style="font-family:basic; margin-top:10px ;">
            Deskripsi :</h6>
            <p style="font-family: basic;">
              
              <?php echo nl2br($produk['description']);?>
            </p>
            <div style="margin-top : 20px">
              <a href="https://api.whatsapp.com/send?phone=6281332249214" target="__blank" class="btn  btn-info btn-sm">
                <i class="fa fa-whatsapp"></i>
                &nbsp;Order
              </a>
              <a href="<?= base_url('products?p=1');?>" class="btn btn-sm btn-warning ">
                <i class="fa fa-archive"></i>
                &nbsp;Product List
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    </section><!-- End About Us Section -->
    </main><!-- End #main -->