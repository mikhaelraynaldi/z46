<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$this->load->view('front/header');
$this->load->view($page);
$this->load->view('front/footer');
