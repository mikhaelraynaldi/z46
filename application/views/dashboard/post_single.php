<table class="table display no-footer" style="width: 100%;">
	<tbody>
		<tr>
			<td width="40%">ID</td>
			<td width="60%"><?php echo $post['id']; ?></td>
		</tr>
		<tr>
			<td>Waktu Pembuatan Publikasi</td>
			<td><?php echo $post['dateproduct']; ?></td>
		</tr>
		<tr>
			<td>Gambar</td>
			<td><img src="<?php echo $post['image_url'];?>" style="height: 50px; height: 50px;"> </td>
		</tr>
		<tr>
			<td>Merek</td>
			<td><?php echo $post['brand']; ?></td>
		</tr>
		<tr>
			<td>Jenis Aki</td>
			<td><?php echo $post['type']; ?></td>
		</tr>
		<tr>
			<td>Nama Aki</td>
			<td><?php echo $post['name']; ?></td>
		</tr>
		<tr>
			<td>Deskripsi</td>
			<td><?php echo nl2br($post['description']); ?></td>
		</tr>
		<tr>
			<td>Harga</td>
			<td> Rp <?php echo number_format($post['price'], 0, ',', '.'); ?></td>
		</tr>
	</tbody>
</table>