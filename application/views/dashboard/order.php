<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">

	<title>ORDER</title>
	<!-- Favicon -->
	<link rel="icon" href="<?= base_url('assets/img/favicon-96x96.png'); ?>" type="image/png">
	<!-- Fonts -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">

	<!-- Icons -->
	<link rel="stylesheet" href="<?= base_url('assets/vendor/nucleo/css/nucleo.css'); ?>" type="text/css">
	<link rel="stylesheet" href="<?= base_url('assets/vendor/@fortawesome/fontawesome-free/css/all.min.css'); ?>" type="text/css">
	<!-- Page plugins -->
	<!-- Argon CSS -->
	<link rel="stylesheet" href="<?= base_url('assets/css/argon.css?v=1.2.0'); ?>" type="text/css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
</head>

<body>
	<!-- Sidenav -->
	<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
		<div class="scrollbar-inner">
			<!-- Brand -->
			<div class="sidenav-header  align-items-center">
				<a class="navbar-brand" href="<?= base_url(); ?>">
					<img src="<?= base_url('assets/img/logo.png'); ?>" class="navbar-brand-img" alt="...">
				</a>
			</div>
			<div class="navbar-inner">
				<!-- Collapse -->
				<div class="collapse navbar-collapse" id="sidenav-collapse-main">
					<!-- Nav items -->
					<ul class="navbar-nav">
						<li class="nav-item">
							<a class="nav-link" href="<?= base_url('dashboard'); ?>">
								<i class="ni ni-tv-2 text-blue"></i>
								<span class="nav-link-text">Dashboard</span>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link active" href="<?= base_url('contactlist'); ?>">
								<i class="ni ni-email-83 text-primary"></i>
								<span class="nav-link-text">Contact List</span>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?= base_url('post'); ?>">
								<i class="ni ni-pin-3 text-primary"></i>
								<span class="nav-link-text">Post</span>
							</a>
						</li>
						<!-- <li class="nav-item">
							<a class="nav-link" href="examples/profile.html">
								<i class="ni ni-single-02 text-yellow"></i>
								<span class="nav-link-text">Profile</span>
							</a>
						</li> -->

					</ul>
				</div>
			</div>
		</div>
	</nav>
	<!-- Main content -->
	<div class="main-content" id="panel">
		<!-- Header -->
		<div class="header bg-primary pb-6">
			<div class="container-fluid">
				<div class="header-body">
					<div class="row align-items-center py-4">
						<div class="col-lg-6 col-7">
							<h6 class="h2 text-white d-inline-block mb-0">Default</h6>
							<nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
								<ol class="breadcrumb breadcrumb-links breadcrumb-dark">
									<li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
									<li class="breadcrumb-item"><a href="#">Dashboards</a></li>
									<li class="breadcrumb-item active" aria-current="page">Contact List</li>
								</ol>
							</nav>
						</div>

					</div>
					<!-- Card stats -->


				</div>
			</div>
		</div>
		<div class="container-fluid mt--6">
			<div class="row">
				<div class="col-xl-12">
					<div class="card">
						<div class="card-header border-0">
							<div class="row align-items-center">
								<div class="col">
									<h3 class="mb-0">ORDER</h3>
								</div>

							</div>
						</div>
						<div class="row">
							<div class="col" style="padding: 0 20px;">
								<div class="table-responsive">
									<table id="ORDER" class="table display" style="width: 100%;">
										<thead>
											<tr>
												<th>ORD NUM</th>
												<th>ORD AMOUNT</th>
												<th>ADVANCE AMOUNT</th>
												<th>ORD DATE</th>
												<th>CUST CODE</th>
												<th>AGENT CODE</th>
												<th>ORD DESCRIPTIOn</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											<?php for ($j = 0; $j < count($order); $j++) :?>
											<tr>
												<td> <?php echo $order[$j]['ORD_NUM'] ;?> </td>
												<td> <?php echo $order[$j]['ORD_AMOUNT'] ;?> </td>
												<td> <?php echo $order[$j]['ADVANCE_AMOUNT'] ;?> </td>
												<td> <?php echo $order[$j]['ORD_DATE'];?> </td>
												<td> <?php echo $order[$j]['CUST_CODE'] ;?> </td>
												<td> <?php echo $order[$j]['AGENT_CODE'] ;?> </td>
												<td> <?php echo $order[$j]['ORD_DESCRIPTION'] ;?> </td>
												<td> <button class="btn btn-sm btn-warning btn-lihat" data-toggle="modal" data-target="#modal-detail" data-id="<?php echo $order[$j]['ORD_NUM'];?>">Lihat</button> </td>
											</tr>
											<?php endfor; ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>		
					</div>
				</div>
			</div>
		</div>

				<!-- Page content -->


			
				<!-- Modal -->
				<div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="modal-postLabel" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="modal-postLabel">Detail</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
							
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
							</div>
						</div>
					</div>
				</div>
		<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
		<script>
			$(document).ready(function() {
				$('#ORDER').DataTable();
			});

			// Function di jalankan ketika
			// element '.btn-warning' di click
			$('.btn-lihat').click(function() {
				// Mendefinisikan variabel data dengan isi :
				// 'id' : (class : $this||.btn-warning ) (Attribute : data-id)
				// untuk dikirimkan menlalui ajax
				let data = {
					ORD_NUM: $(this).attr('data-id')
				}
			
			// Request Ajax
			$.ajax({
				// url untuk mengirimkan ke tujuan yang di request
				url: 'http://localhost/z46/order/ajax_get_order_html',

				// Tipe Request Ajax (get)
				type: 'get',

				// Mengirimkan variabel date_reques melalui ajax
				data : data,
			

				// Function yang di jalankan jika request 'success'
				// menerima parameter 'res' dari return ajax
				success: function(res){
					// 1. mencari '#modal-detail'
					// 2. di dalam '#modal-detail', cari element '.modal-body'
					// 3. jalankan fungsi 'html' untuk mengganti kontent di dalam elemenet '.modal-body'
					// dengan variabel 'res'
						$('#modal-detail').find('.modal-body').html(res);
				}
			});
		});
		</script>
		<!-- Argon Scripts -->
		<!-- Core -->
		<script src="<?= base_url('assets/vendor/jquery/jquery.min.js'); ?>"></script>
		<script src="<?= base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
		<script src="<?= base_url('assets/vendor/js-cookie/js.cookie.js'); ?>"></script>
		<script src="<?= base_url('assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js'); ?>"></script>
		<script src="<?= base_url('assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js'); ?>"></script>
		<!-- Optional JS -->
		<script src="<?= base_url('assets/vendor/chart.js/dist/Chart.min.js'); ?>"></script>
		<script src="<?= base_url('assets/vendor/chart.js/dist/Chart.extension.js'); ?>"></script>

		<!-- Argon JS -->
		<script src="<?= base_url('assets/js/argon.js?v=1.2.0'); ?>"></script>



		<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>

</body>

</html>


								</div>
							</div>
						</div>