	</div>
	
	<!-- Argon Scripts -->
	<!-- Core -->
	<script src="<?= base_url('assets/vendor/jquery/jquery.min.js'); ?>"></script>
	<script src="<?= base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
	<script src="<?= base_url('assets/vendor/js-cookie/js.cookie.js'); ?>"></script>
	<script src="<?= base_url('assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js'); ?>"></script>
	<script src="<?= base_url('assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js'); ?>"></script>
	<!-- Optional JS -->
	<script src="<?= base_url('assets/vendor/chart.js/Chart.min.js'); ?>"></script>
	<script src="<?= base_url('assets/vendor/chart.js/Chart.extension.js'); ?>"></script>

	<!-- Argon JS -->
	<script src="<?= base_url('assets/js/argon.js?v=1.2.0'); ?>"></script>

	<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>

	</body>

	</html>