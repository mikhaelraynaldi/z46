<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
		<title>Dashboard <?php echo isset($title) ? '| ' . $title : '';?>  </title>
		<!-- Favicon -->
		<link rel="icon" href="<?= base_url('assets/img/favicon-96x96.png'); ?>" type="image/png">
		<!-- Fonts -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
		<!-- Icons -->
		<link rel="stylesheet" href="<?= base_url('assets/vendor/nucleo/css/nucleo.css'); ?>" type="text/css">
		<link rel="stylesheet" href="<?= base_url('assets/vendor/@fortawesome/fontawesome-free/css/all.min.css'); ?>" type="text/css">
		<!-- Page plugins -->
		<!-- Argon CSS -->
		<link rel="stylesheet" href="<?= base_url('assets/css/argon.css?v=1.2.0'); ?>" type="text/css">
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
		<link rel="stylesheet" href="<?= base_url('assets/css/custom.css'); ?>" type="text/css">
	</head>
	<body>
		<!-- Sidenav -->
		<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
			<div class="scrollbar-inner">
				<!-- Brand -->
				<div class="sidenav-header  align-items-center">
					<a class="navbar-brand" href="<?= base_url(); ?>">
						<img src="<?= base_url('assets/img/logo.png'); ?>" class="navbar-brand-img" alt="...">
					</a>
				</div>
				<div class="navbar-inner">
					<!-- Collapse -->
					<div class="collapse navbar-collapse" id="sidenav-collapse-main" style="justify-content: flex-end;">
						<!-- Nav items -->
						<ul class="navbar-nav">
							<li class="nav-item">
								<a class="nav-link" id="dashboard" href="<?= base_url('dashboard'); ?>">
									<i class="ni ni-tv-2 text-primary"></i>
									<span class="nav-link-text">Dashboard</span>
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="contact_list" href="<?= base_url('contactlist'); ?>">
									<i class="ni ni-email-83 text-primary"></i>
									<span class="nav-link-text">Contact List</span>
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="post" href="<?= base_url('post'); ?>">
									<i class="ni ni-book-bookmark text-primary"></i>
									<span class="nav-link-text">Post</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</nav>
		<div class="main-content" id="panel">
			<nav class="navbar navbar-top navbar-expand navbar-dark bg-primary border-bottom">
				<div class="container-fluid">
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav align-items-center ml-auto ml-md-0">
							<li class="nav-item dropdown">
								<a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<div class="media align-items-center">
										<span class="avatar avatar-sm rounded-circle">
											<img src="https://via.placeholder.com/50x50">
										</span>
										<div class="media-body  ml-2  d-none d-lg-block">
											<span class="mb-0 text-sm  font-weight-bold">
												<?php echo ucwords($this->session->userdata('full_name')); ?>
											</span>
										</div>
									</div>
								</a>
								<div class="dropdown-menu  dropdown-menu-right ">
									<!-- <div class="dropdown-header noti-title">
										
									</div> -->
									<!-- <a href="#!" class="dropdown-item">
										<i class="ni ni-single-02"></i>
										<span>My profile</span>
									</a>
									<a href="#!" class="dropdown-item">
										<i class="ni ni-settings-gear-65"></i>
										<span>Settings</span>
									</a>
									<a href="#!" class="dropdown-item">
										<i class="ni ni-calendar-grid-58"></i>
										<span>Activity</span>
									</a>
									<a href="#!" class="dropdown-item">
										<i class="ni ni-support-16"></i>
										<span>Support</span>
									</a>
									<div class="dropdown-divider"></div> -->
									<a href="<?php echo base_url('login/close'); ?>" class="dropdown-item">
										<i class="ni ni-user-run"></i>
										<span>Logout</span>
									</a>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</nav>