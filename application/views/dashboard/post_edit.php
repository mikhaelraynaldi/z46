
	<!-- Main content -->
	<div class="main-content" id="panel">

		<!-- Header -->
		<div class="header bg-primary pb-6">
			<div class="container-fluid">
				<div class="header-body">
					<div class="row align-items-center py-4">
						<div class="col-lg-6 col-7">
							<h6 class="h2 text-white d-inline-block mb-0">Default</h6>
							<nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
								<ol class="breadcrumb breadcrumb-links breadcrumb-dark">
									<li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
									<li class="breadcrumb-item"><a href="#">Dashboards</a></li>
									<li class="breadcrumb-item active" aria-current="page">Post Edit</li>
								</ol>
							</nav>
						</div>
					</div>
					<!-- Card stats -->


				</div>
			</div>
		</div>
		<div class="container-fluid mt--6">
			<div class="row">
				<div class="col-xl-12">
					<div class="card">
						<div class="card-header border-0">
							<div class="row align-items-center">
								<div class="col">
									<h3 class="mb-0">Edit Post Produk</h3>
								</div>


							</div>
						</div>
						<div class="row">
							<div class="col" style="padding: 0 50px;">
								<form id="post_product" method="post" class="php-email-form">
									<input type="hidden" name="id" value="<?php echo $post['id']; ?>">

									<div class="form-group">
										<label for="example-text-input" class="form-control-label">Merek</label>
										<input class="form-control" type="text" name="brand" placeholder="Contoh : GS ASTRA" value="<?php echo $post['brand'] ?>">
									</div>
									<div class="form-group">
										<label for="example-text-input" class="form-control-label">Jenis Aki</label>
										<input class="form-control" type="text" name="type" placeholder="Contoh : Aki Mobil" value="<?php echo $post['type']; ?>">
									</div>
									<div class="form-group">
										<label for="example-text-input" class="form-control-label">Nama Aki</label>
										<input class="form-control" type="text" name="name" placeholder="Contoh :GS Hybrid Tipe Ns40" value="<?php echo $post['name']; ?>">
									</div>
									<div class="form-group">
										<label for="example-search-input" class="form-control-label">Gambar</label><br>
										<img src="<?php echo $post['image_url'];  ?>" style="height: 200px;">
										<input class="btn-sm" type="file" accept=".jpeg, .jpg,.png"  name="new_image" value="" id="gambar_produk">
									</div>
									<div class="form-group">
										<label for="example-text-input" class="form-control-label">Deskripsi</label>
										<textarea class="form-control" name="description" placeholder="Deskripsi" rows="15"><?php echo $post['description']; ?></textarea>
									</div>
									<div class="form-group">
										<label class="form-control-label">Harga</label>
										<input class="form-control" name="price" placeholder="Contoh :20000" value="<?php echo $post['price']; ?>">
									</div>
									<div class="form-group">
										<button class="btn-sm btn btn-success" type="submit">Save</button>
										<button id="cancel_button" class="btn btn-sm btn-danger">Cancel</button>
									</div>

								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
		<script src="<?= base_url('assets/js/z46/post_edit.js');?>"></script>