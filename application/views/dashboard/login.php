<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>Aki Terdekat | Login</title>
  <link rel="icon" href="<?= base_url('assets/img/favicon-96x96.png'); ?>" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="<?= base_url('assets/vendor/nucleo/css/nucleo.css'); ?>" type="text/css">
  <link rel="stylesheet" href="<?= base_url('assets/vendor/@fortawesome/fontawesome-free/css/all.min.css'); ?>" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="<?= base_url('assets/css/argon.css?v=1.2.0'); ?>" type="text/css">
</head>

<body class="bg-default">
  <!-- Navbar -->
  <!-- Main content -->
  <div class="main-content" style="margin-top: 200px;">
    <!-- Header -->
    <!-- Page content -->
    <div class="container mt--8 pb-5">
      <div class="row justify-content-center">
        <div class="col-lg-5 col-md-7">
          <div class="card bg-secondary border-0 mb-0">

            <div class="card-body px-lg-5 py-lg-5">
              <div class="text-center text-muted mb-4">
                <H2>Login</H2>
              </div>

              <!-- Menampilkan login error jika nilai tidak true -->
              <?php if($this->session->userdata('login_error_message')) : ?>
                <div class="text-center">
                  <p style="color: red">Username atau password yang diinput salah, silakan coba lagi.</p>
                </div>
              <?php endif; ?>

              <form role="form" action="<?= site_url('login/validate_login');?>" method="post">
                <div class="form-group mb-3">
                  <div class="input-group input-group-merge input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-single-02"></i></span>
                    </div>
                    <input class="form-control" placeholder="Username" name="username" required>
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group input-group-merge input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                    </div>
                    <input class="form-control md-5" placeholder="Password" type="password" name="password" required>
                  </div>
                </div>
                <div class="text-center">
                  <button type="submit" class="btn btn-primary my-4">Submit</button>
                </div>
              </form>
            </div>
          </div>
          <div class="row mt-3">
            <!-- <div class="col-6">
              <a href="#" class="text-light"><small>Forgot password?</small></a>
            </div> -->

          </div>
        </div>
      </div>
    </div>
  </div>

  <script src="<?= base_url('/assets/vendor/jquery/jquery.min.js');?>"></script>
  <script src="<?= base_url('/assets/vendor/bootstrap/js/bootstrap.bundle.min.js');?>"></script>
  <script src="<?= base_url('/assets/vendor/js-cookie/js.cookie.js');?>"></script>
  <script src="<?= base_url('/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js');?>"></script>
  <script src="<?= base_url('/assets/vendor/jquery-scroll-lock/jquery-scrollLock.min.js');?>"></script>
  <!-- Argon JS -->
  <script src="<?= base_url('/assets/js/argon.js?v=1.2.0');?>"></script>
</body>

</html>