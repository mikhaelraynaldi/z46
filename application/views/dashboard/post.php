
	<!-- Main content -->
	<div class="main-content" id="panel">

		<!-- Header -->
		<div class="header bg-primary pb-6">
			<div class="container-fluid">
				<div class="header-body">
					<div class="row align-items-center py-4">
						<div class="col-lg-6 col-7">
							<h6 class="h2 text-white d-inline-block mb-0">Default</h6>
							<nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
								<ol class="breadcrumb breadcrumb-links breadcrumb-dark">
									<li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
									<li class="breadcrumb-item"><a href="#">Dashboards</a></li>
									<li class="breadcrumb-item active" aria-current="page">Post</li>
								</ol>
							</nav>
						</div>

					</div>
					<!-- Card stats -->


				</div>
			</div>
		</div>
		<div class="container-fluid mt--6">
			<div class="row">
				<div class="col-xl-12">
					<div class="card">
						<div class="card-header border-0">
							<div class="row align-items-center">
								<div class="col">
									<h3 class="mb-0">Post Produk</h3>
								</div>
								<div class="col text-right">
									<a href="<?= base_url('post/post_create'); ?>" class="btn btn-sm btn-primary">New Post</a>
								</div>

							</div>
						</div>
						<div class="row">
							<div class="col" style="padding: 0 20px;">
								<div class="table-responsive">
									<table id="produk_post" class="table display" style="width: 100%;">
										<thead>
											<tr>
												
												<th>Merek</th>
												<th>Gambar</th>
												<th>Jenis Aki</th>
												<th>Nama Aki</th>
												<th>Harga</th>
												<th>Aksi</th>
											</tr>
										</thead>
										<tbody>
											<!-- JQUERY: parent() dan find('.btn-hapus) -->
											<?php for ($j = 0; $j < count($product); $j++) : ?>
												<tr>
													
													<td>
														<?php echo $product[$j]['brand']; ?>
													</td>
													<td>
														<img src="<?php echo $product[$j]['image_url'];?>" style="height: 50px; width:50px;">
													</td>
													<td>
														<?php echo $product[$j]['type']; ?>
													</td>
													<td>
														<?php echo $product[$j]['name']; ?>
													</td>
													<td>
														RP
														<?php echo number_format($product[$j]['price'],0, ',', '.'); ?>
													</td>
													<td>
														<button class="btn btn-sm btn-warning btn-lihat" data-toggle="modal" data-target="#modal-post" data-id="<?php echo $product[$j]['id']; ?>">
															<i class="fas fa-eye"></i>
														</button>
														<a href="<?php echo base_url('post/post_edit/' . $product[$j]['id']) ?>" class="btn btn-info btn-sm">
															<i class="fas fa-edit"></i>
														</a>
														<!-- menampung id_produk pada button hapus -->
														<button class="btn btn-danger btn-sm btn-hapus" data-product-id="<?php echo $product[$j]['id'], $product[$j]['image_url']; ?>">
															<i class="fas fa-trash-alt"></i>
														</button>
													</td>
												<?php endfor; ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Modal -->
				<div class="modal fade" id="modal-post" tabindex="-1" role="dialog" aria-labelledby="modal-postLabel" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="modal-postLabel">Deskripsi</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
							
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
							</div>
						</div>
					</div>
				</div>


				<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
				<script src="<?= base_url('assets/js/z46/post.js');?>"></script>
			