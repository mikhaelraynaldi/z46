	<!-- Main content -->
	<div class="main-content" id="panel">

		<!-- Header -->
		<div class="header bg-primary pb-6">
			<div class="container-fluid">
				<div class="header-body">
					<div class="row align-items-center py-4">
						<div class="col-lg-6 col-7">
							<h6 class="h2 text-white d-inline-block mb-0">Default</h6>
							<nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
								<ol class="breadcrumb breadcrumb-links breadcrumb-dark">
									<li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
									<li class="breadcrumb-item"><a href="#">Dashboards</a></li>
									<li class="breadcrumb-item active" aria-current="page">Post Create</li>
								</ol>
							</nav>
						</div>

					</div>
					<!-- Card stats -->


				</div>
			</div>
		</div>
		<div class="container-fluid mt--6">
			<div class="row">
				<div class="col-xl-12">
					<div class="card">
						<div class="card-header border-0">
							<div class="row align-items-center">
								<div class="col">
									<h3 class="mb-0">Post Products</h3>
								</div>


							</div>
						</div>
						<div class="row">
							<div class="col" style="padding: 0 50px;">
								<form id="post_product" method="post" class="php-email-form">
									<div class="form-group">
										<label for="example-text-input" class="form-control-label">Merek</label>
										<input class="form-control" type="text" name="merekproduk" placeholder="Contoh : GS ASTRA">
									</div>
									<div class="form-group">
										<label for="example-text-input" class="form-control-label">Jenis Aki</label>
										<input class="form-control" type="text" name="tipeproduk" placeholder="Contoh : Aki Mobil">
									</div>
									<div class="form-group">
										<label for="example-text-input" class="form-control-label">Nama Aki</label>
										<input class="form-control" type="text" name="namaproduk" placeholder="Contoh :GS Hybrid Tipe Ns40">
									</div>
									<div class="form-group">
										<label for="example-search-input" class="form-control-label">Gambar</label><br>
										<input class="btn-sm" type="file" accept=".jpeg, .jpg,.png" id="gambar_produk">
									</div>
									<div class="form-group">
										<label for="example-text-input" class="form-control-label">Deskripsi</label>
										<textarea class="form-control" name="deskripsi" placeholder="Deskripsi" rows="15"></textarea>

									</div>

									<div class="form-group">
										<label class="form-control-label">Harga</label>
										<input class="form-control" name="harga" placeholder="Contoh :20000">
									</div>
									<div class="form-group">
										<button class="btn-sm btn btn-success" type="submit">Save</button>
										<button id="cancel_button" class="btn btn-sm btn-danger">Cancel</button>
									</div>

								</form>
							</div>
						</div>
					</div>
				</div>

				<!-- Page content -->


				<!-- Footer -->
				<footer class="footer pt-0">
					<div class="row align-items-center justify-content-lg-between">
						<div class="col-lg-6">
							<div class="copyright text-center  text-lg-left  text-muted">

							</div>
						</div>
						<div class="col-lg-6">

						</div>
					</div>
				</footer>
			</div>
		</div>
		<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
		<script src="<?= base_url('assets/js/z46/post_create.js');?>"></script>
		